﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hp : MonoBehaviour
{
    public float hpInt;
    public float timer;

    private void Update()
    {
        timer += Time.deltaTime;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Punch")
        {
            if (timer > 1)
            {
                timer = 0;
                hpInt -= 20;
            }
        }
    }
}
