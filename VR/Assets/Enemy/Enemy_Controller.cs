using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy_Controller : MonoBehaviour
{
    public float hp = 50f;
    public Transform target;
    public float distance;
    public NavMeshAgent NavMeshAgentEnemy;
    public Animator animator;
    public GameObject die;

    public void Start()
    {
        animator = GetComponent<Animator>();
        NavMeshAgentEnemy = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        distance = Vector3.Distance(transform.position, target.transform.position);
        if (distance > 20)
        {
            NavMeshAgentEnemy.enabled = false;
            animator.SetBool("Idle", true);
            animator.SetBool("run", false);
            animator.SetBool("Attack", false);
        }
        if (distance <= 20 && distance > 1.5f)
        {
            NavMeshAgentEnemy.enabled = true;
            NavMeshAgentEnemy.SetDestination(target.position);
            animator.SetBool("Idle", false);
            animator.SetBool("run", true);
            animator.SetBool("Attack", false);
        }
        if (distance <= 1.5f)
        {
            NavMeshAgentEnemy.enabled = false;
            animator.SetBool("Idle", false);
            animator.SetBool("run", false);
            animator.SetBool("Attack", true);
        }
        if (hp <= 0.1)
        {
            //Instantiate(die, gameObject.transform.position, gameObject.transform.rotation);
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "bullet")
        {
            hp -= 10f;
        }
    }
}