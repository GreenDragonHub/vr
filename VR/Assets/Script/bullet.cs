﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    public float speed;
    public float distanse;
    public int damage;
    public LayerMask LayerMask;


    void Update()
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, transform.right, distanse, LayerMask);

        transform.Translate(Vector2.right * speed * Time.deltaTime);
    }
}
