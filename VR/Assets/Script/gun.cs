﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class gun : MonoBehaviour
{
    public SteamVR_Action_Boolean fireAction;
    public GameObject bellet;
    public Transform barrelPivot;
    public GameObject muzzleFlassh;
    public float shootingSpeed = 1;

    private Animator animator1;
    private Interactable interactable;

    void Start()
    {
        animator1 = GetComponent<Animator>();
        muzzleFlassh.SetActive(false);
        interactable = GetComponent<Interactable>();
    }
    void Update()
    {
        if(interactable.attachedToHand != null)
        {
            SteamVR_Input_Sources source = interactable.attachedToHand.handType;
            if (fireAction[source].stateDown)
            {
                Fire();
            }
        }
    }
    public void Fire()
    {
        Rigidbody bulletRigidbody = Instantiate(bellet, barrelPivot.position, barrelPivot.rotation).GetComponent<Rigidbody>();
        bulletRigidbody.velocity = barrelPivot.forward * shootingSpeed;
        muzzleFlassh.SetActive(false);
    }
}
